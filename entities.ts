export interface Product{
    id?: number;
    title?: string;
    description?:string;
    price?: number;
    stocks?: number;
    images:Image[];
}

export interface Image{
    id?:number;
    link: string;
}
// faire une interface lineProduct et une interface product
export interface Order{
    id?: number;
    title?: string;
    detail?: string;
    total?: number;
    status?: string;
    lineProduct?: LineProduct[];
}

export interface LineProduct{
    id?: number;
    title?: string;
    quantity?: number;
    custom?: boolean;
    price?: number;
    product?: Product;
}
