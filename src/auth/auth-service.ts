import { Adress, Order, User } from "@/entities";
import axios from "axios";



export async function login(mail: string, password: string) {
    const response = await axios.post<{ token: string }>('/api/login', { mail, password });
    return response.data.token;
}

export async function fetchUser() {
    const response = await axios.get<User>('/api/account');
    return response.data;
}

export async function fetchUserAdresses() {
    const response = await axios.get<Adress[]>('/api/account/adress');
    return response.data;
}

export async function fetchUserorder() {
    const response = await axios.get<Order[]>('/api/account/orders');
    return response.data;
}

