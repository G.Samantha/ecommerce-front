import { Order } from "@/entities";

interface Props {
  order: Order;
}

export default function CardOrderDetail({ order }: Props) {
  return (
    <>
      <div className="d-flex card m-3" style={{ width: 18 + "rem" }}>
        <div className="card-body bg-light">
          <p className="mb-0">{order.title}</p>
          <p key={order.id}></p>
          <p className="mb-0">{order.status}</p>
          <ul>
            <li>{order.detail}</li>
          </ul>
          <p className="mb-0">{order.total}</p>
        </div>
      </div>
    </>
  );
}
