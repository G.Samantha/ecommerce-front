import { Order } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Button from "@mui/material/Button";
import { deleteFromCart } from "@/cart-service";


interface Props {
    order: Order;
}

export default function ItemCart({ order }: Props) {

    const routeur = useRouter();

    const handleEmptyCart = () => {
        if (order.lineProducts?.length === 0) {
            routeur.push("/empty-cart");
        } else {
            order.status = "en cours";
            routeur.push("/");
        }
    };

    const [step, setStep] = useState(0);
    const [cartTotal, setCartTotal] = useState(0);
    useEffect(() => {
        const total = order?.lineProducts?.reduce((acc, line) => acc + (line.price * line.quantity), 0);
        if (total) {

            setCartTotal(total);
        }
    }, [order]);

    return (
        <>
            <div className="cartcard">
                <div className="row">
                    <div className="col-md-8 cart">
                        <div className="title">
                            <div className="row">
                                <div className="col"><h4><b>Shopping Cart</b></h4></div>
                                <div className="col align-self-center text-right text-muted">
                                    <h3>{order?.lineProducts?.length} Produits</h3></div>
                            </div>
                        </div>

                        {order?.lineProducts?.map(line =>
                            <>
                                <li key={line.id}>

                                    <div className="row border-top border-bottom">
                                        <div className="row cartmain align-items-center">
                                            <div className="col-2">
                                                <img className=" img-fluid" src={line.image} /></div>
                                            <div className="col-3">
                                                <div className="row text-muted">{line.title}</div>
                                            </div >
                                            <div className="col-2">
                                                <div className="row d-flex justify-content-around align-item-center">
                                                    <div className="col text-center p-0 ">
                                                    <button type="button" className="btn btn-light" onClick={() => setStep(line.quantity -= 1)}>-</button>
                                                    </div>
                                                    <div className="col text-center p-0 ">
                                                <p className="mb-0">{line.quantity}</p>
                                                </div>
                                                <div className="col text-center p-0 ">
                                                <button type="button" className="btn btn-light" onClick={() => setStep(line.quantity += 1)}>+</button>
                                                </div>
                                                </div>
                                            </div>
                                            <div className="col text-right">&euro; {line.price * line.quantity}</div>
                                            <div className="col text-right">
                                            <button type="button" className="btn btn-danger" onClick={() => {deleteFromCart(line); routeur.push("/");}}>X</button>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </>
                        )}
                        <div className="back-to-shop"><a href="#"><ArrowBackIcon/></a><span className="text-muted">Back to shop</span></div>

                    </div>
                    <div className="col-md-4 summary">
                        <div><h5><b>Summary</b></h5></div>
                        <hr />
                        <div className="row">
                            <div className="col" style={{ paddingLeft: 10 }}>{order?.lineProducts?.length} Produits</div>
                            <div className="col text-right">&euro; {(cartTotal).toFixed(2)}</div>
                        </div>
                        <form className="cartform">
                            <p>SHIPPING</p>
                            <select className="cartselect"><option className="text-muted">Frais-de-Livraison- &euro;5.00</option></select>
                            <p>ENTREZ VOTRE CODE</p>
                            <input className="cartinput" id="code" placeholder="Enter your code" />
                        </form>
                        <div className="row" style={{ borderTop: "1px solid rgba(0,0,0,.1)", padding: "2vh 0" }} >
                            <div className="col">PRIX TOTAL</div>
                            <div className="col text-right">&euro; {(cartTotal).toFixed(2)}</div>
                        </div>
                        <button className="cartbtn" onClick={() => handleEmptyCart()}>Valider</button>
                    </div>
                </div>

            </div>
        </>
    )
}