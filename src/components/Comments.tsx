import { AuthContext } from "@/auth/auth-context";
import { postComment } from "@/comment-service";
import { Comment, Product} from "@/entities";
import router, { useRouter } from "next/router";
import React, { useContext, useState } from "react";
import FormComment from "./FormComment";

interface Props {
    comments: Comment[];
}

export default function Comments({ comments }: Props) {
    
    
    return (
        <>

            <div className="row justify-content-center">
            <div  className="col-10">
            <h3 className="mt-4 mb-4">Commentaires</h3>
            </div>
            <div className="list-group col-10 mb-4">
            {comments?.map(comment =>
           
            <div key={comment.id} className="list-group-item " >
            <div className="d-flex w-100 justify-content-between">
            <h5 className="mb-1">"{comment.content}"</h5>
            <small>{(comment.date)?.slice(0,10)}</small>
            </div>
            <small>{comment.author}</small>
            <p className="mb-1"> Note: {Number(comment.rate)}/5 </p>
            <img src={comment.linkImgComment} alt="" />
           
        
            </div>
             )}
            </div>
            
            </div>
            
            </>
            )
            
        }