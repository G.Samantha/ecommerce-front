import { FormEvent, useState } from "react";
import { Adress } from "../entities";
import { Button, Card, Form, Input } from "antd";
// import { deleteAdress } from "@/adress-services";
import { useRouter } from "next/router";

interface Props {
  onSubmit: (adress: Adress) => void;
  // onDelete: (id:any)=>void;
  edited?: Adress;
}

export default function FormEditAdress({ onSubmit, edited }: Props) {
  const [errors, setErrors] = useState("");
  const router = useRouter();
  // const handleDelete =()=>{
  //   onDelete(adresses.id);
  // }

  const [adresses, setAdresses] = useState<Adress>(
    edited
      ? edited
      : {
          streetNum: "",
          street: "",
          zipCode: "",
          city: "",
        }
  );

  // async function handleDelete(adresses: Adress) {
  //   await deleteAdress(adresses);
  // }

  async function handleSubmit(adress: Adress) {
    try {
      onSubmit({ ...adresses, ...adress });
    } catch (error: any) {
      console.log(error);

      if (error.response.status == 400) {
        setErrors(error.response.data.detail);
      }
    }
  }

  return (
    <>
      <Card
        style={{ width: "35%", margin: "auto", marginTop: "20px" }}
        className="card bg-light"
      >
        <h1>Mon adresse</h1>
        <Form onFinish={handleSubmit} initialValues={adresses}>
          <Form.Item
            label="Numéro de rue"
            name="streetNum"
            rules={[
              {
                required: false,
                message: "Please input your streetnum!",
              },
            ]}
          >
            <Input type="streetnum" />
          </Form.Item>

          <Form.Item
            label="Rue"
            name="street"
            rules={[{ required: false, message: "Please input your street!" }]}
          >
            <Input type="name" />
          </Form.Item>

          <Form.Item
            label="Code postale"
            name="zipCode"
            rules={[{ required: false, message: "Please input your zipcode!" }]}
          >
            <Input type="number" />
          </Form.Item>

          <Form.Item
            label="ville"
            name="city"
            rules={[{ required: false, message: "Please input your city!" }]}
          >
            <Input type="name" />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Modifier
            </Button>
          </Form.Item>
          {/* <Form.Item >
                  <Button type="primary" htmlType="submit" onClick={handleDelete} >
                    Supprimer
                  </Button>
                </Form.Item> */}
        </Form>
      </Card>
    </>
  );
}
