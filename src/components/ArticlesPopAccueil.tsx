import { Product } from "@/entities";
import React from "react";

interface Props {
  products: Product;
}

export default function articlesPopAccueil({ products }: Props) {
  return (
    <>
      <div className="card border border-0" style={{ width: 18 + "rem" }}>
        <img
          src="https://images.pexels.com/photos/2118483/pexels-photo-2118483.jpeg?auto=compress&cs=tinysrgb&w=600"
          className="card-img-top"
          alt="img"
        />
        <div className="card-body">
          <p className="card-text d-flex justify-content-center">
            {products.title}
          </p>
          <p className="card-text d-flex justify-content-center">
            {products.price} €
          </p>
        </div>
      </div>
    </>
  );
}
