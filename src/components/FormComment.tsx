import { Comment } from "@/entities";
import { Button } from "@mui/material";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";
import PhotoCamera from '@mui/icons-material/PhotoCamera';


interface Props {
    onSubmit:(comment:Comment) => void;
    edited?:Comment;
}


export default function FormComment({onSubmit, edited}:Props) {
    
    const router = useRouter();
    
    const [errors, setErrors] = useState('');
    
    const [comment, setComment] = useState<Comment>(edited?edited:{
        'rate': 5,
        'author': "",
        'content': "",
    });
   
    
    function handleChange(event: any) {
        setComment({
            ...comment,
            [event.target.name]: event.target.value
        });
    }
    
    function handleFile(event: any) {
        const reader = new FileReader();
        reader.onload = () => setComment({
            ...comment,
            image: String(reader.result)
        });

        reader.readAsDataURL(event.target.files[0]);
    }




    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        try {
            onSubmit(comment);
            
        } catch(error:any) {
            if(error.response.status == 400) {
                setErrors(error.response.data.detail);
            }
        }
    }
    
    return (
        <div className="row d-flex justify-content-center">
        <div className="col-sm-7 col-md-8 formulaire ">
        
        <form onSubmit={handleSubmit} className="mt-3 mb-4">
        {errors && <p>{errors}</p>}
        <div className="mt-3 mb-3">
        <label htmlFor="author"className="form-label">Votre pseudo:</label>
        <input type="text" name="author" className="form-control" value={comment.author} onChange={handleChange} required/>
        </div>
        <div className="mt-3 mb-3">
        <label htmlFor="content" className="form-label">Votre commentaire:</label>
        <textarea name="content" className="form-control" value={comment.content} onChange={handleChange} required/>
        <div className="mt-3 mb-3">
        <label htmlFor="rate" className="form-label">Votre note:</label>
        <input type="number" max="5" name="rate" className="form-control" value={comment.rate} onChange={handleChange} required/>
        </div>
    
        </div>
        <div>
        {comment.image &&
                            <div>
                                <img src={comment.image} style={{ height: 400 }} />
                            </div>
                        }
                        <Button color="info" variant="contained" component="label" className="btn btn-primary mt-3 mb-3" endIcon={ <PhotoCamera />} >
                            Ajouter une image <input
                                type="file"
                                hidden
                                name="src"
                                onChange={handleFile}  /></Button>
                    </div>
                    <Button color="info" variant="contained" type="submit" className="btn btn-primary mt-4 mb-4">Valider</Button>
        
        </form>
        
        
        </div>
        </div>
        
        
        
        )
    }
    