import SearchIcon from '@mui/icons-material/Search';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { Category } from '@/entities';
import { useEffect, useState } from 'react';
import { fetchAllCategory } from '@/category-services';
import { useRouter } from 'next/router';



export default function Navbar() {
  const [categorys, setCategorys] = useState<Category[]>();
  const router = useRouter();
  
  useEffect(() => {
    fetchAllCategory().then(data => {
      setCategorys(data);
    }).catch(error => {
      if(error.response.status == 401) {
        router.push('/login')
      }
    });
  }, [])
  
  return (
    <>
    <div className="container-fluid bg-warning">
    <div  className="row d-flex justify-conter-around">
    <div className="col-4 mt-3 text-center"> </div>
    <div className="col-sm-12 col-md-4  mt-3 text-center">
    <a href="/" className="my-link">
    <img src="/logo.png" alt="Logo" height="60" className="d-inline-block align-text-center"/> 
    </a>
    </div>
    <div className="col-sm-12 col-md-4  mt-3 text-end">
    <a href="/" className="ms-2 me-2 my-link">
    <SearchIcon />
    </a>
    <a href="/account" className="ms-2 me-2 my-link">
    <AccountCircleIcon  />
    </a>
    <a href="/cart" className="ms-2 me-2 my-link">
    <ShoppingCartIcon />
    </a>
    </div>
    
    </div>
    <nav className="navbar navbar-expand-lg bg-warning">
    <div
    className="row d-flex justify-content-center collapse navbar-collapse"
    id="navbarText"
    >
    <div className="col-12 col-lg-4">
    <button
    className="navbar-toggler"
    type="button"
    data-bs-toggle="collapse"
    data-bs-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent"
    aria-expanded="false"
    aria-label="Toggle navigation"
    >
    <span className="navbar-toggler-icon"></span>
    </button>
    <div
    className="collapse navbar-collapse justify-content-center"
    id="navbarSupportedContent"
    >
    <ul className="navbar-nav justify-content-center">
    {categorys &&
      categorys.map(category =>
        <li key={category.id} className="nav-item">
        <a className="nav-link active" aria-current="page" href={"/category/" + category.id}>{category.name}</a>
        </li>)}
        </ul>
        </div>
        </div>
        </div>
        </nav>
        </div>
        </>
        );
      }
      