import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';

export default function Footer() {
  return (
    <>
      <footer className="position-relative">
        <div className="container-fluid ">
          <div className="row d-flex justify-content-between bg-warning p-3">
            <div className="col-md-3 col-sm-3">
            <a href="/" className="my-link">
              <img src="/logo.png" alt="Logo" height="60" className="d-inline-block align-text-center"/> 
            </a>
            </div>
            <div className="col-md-4 col-sm-6 m-2 text-center">
              <button type="button" className="btn btn-light">
                Contactez-nous
              </button>
            </div>
            <div className="col-md-3 col-sm-3 text-end ">
              <a
                className="btn btn-link btn-floating text-dark"
                href="https://www.instagram.com"
                role="button"
                data-mdb-ripple-color="dark"
              >
                <InstagramIcon/>
              </a>
              <a
                className="btn btn-link btn-floating text-dark"
                href="https://fr-fr.facebook.com/"
                role="button"
                data-mdb-ripple-color="dark"
              >
                <FacebookIcon />
              </a>
            </div>
          </div>
        </div>

        <div className="text-center text-dark p-3 bg-warning">
          © 2020 Chems Laure Yann Samantha:
          <a className="text-dark p-1" href="">
            Mentions légales
          </a>
          <a className="text-dark p-1" href="">
            RGPD
          </a>
        </div>
      </footer>
    </>
  );
}
