import { Button, Card, Form, Input } from "antd";
import { useState } from "react";
import { User } from "../entities";

interface Props {
  onSubmit: (user: User) => void;
  edited?: User;
}

export default function FormEditUserAccount({ onSubmit, edited }: Props) {
  const [error, setError] = useState("");

  const [UserAccount, setUserAccount] = useState<User>(
    edited
      ? edited
      : {
          name: "",
          lastname: "",
          mail: "",
          phone: "",
          password: "",
          adresses: [],
          orders: [],
        }
  );

  async function handleSubmit(user: User) {
    try {
      onSubmit({ ...UserAccount, ...user });
    } catch (error: any) {
      console.log(error);

      if (error.response.status == 400) {
        setError(error.response.data.detail);
      }
    }
  }

  return (
    <>
      <h2 className="mt-3">Modifier mes informations</h2>
      <Card
        style={{ minWidth:"auto", marginTop: "20px"  }}
        className="card bg-light"
      >
        <Form onFinish={handleSubmit} initialValues={UserAccount}>
          <Form.Item
            label="Nom"
            name="name"
            rules={[{ required: false, message: "Please input your name!" }]}
          >
            <Input type="name" />
          </Form.Item>

          <Form.Item
            label="Prénom"
            name="lastname"
            rules={[
              { required: false, message: "Please input your lastName!" },
            ]}
          >
            <Input type="lastname" />
          </Form.Item>

          <Form.Item
            label="Téléphone"
            name="phone"
            rules={[{ required: false, message: "Please input your phone!" }]}
          >
            <Input type="phone" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              Modifier
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </>
  );
}
