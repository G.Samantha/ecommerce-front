import axios from "axios";
import { Order, Product } from "../entities";

export async function fetchCartProduct() {
    const response = await axios.get<Order>('/api/cart/current');

    return response.data;
}