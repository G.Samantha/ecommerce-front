export interface Product {
    id?: number;
    title?: string;
    description?: string;
    price?: number;
    stocks?: number;
    images?: Image[];
    comments?: Comment[];
}
export interface User {
    id?: number;
    name?: string;
    lastname?: string;
    mail: string;
    password: string;
    phone?: string;
    role?: string
    adresses: Adress[];
    orders: Order[];
}

export interface Adress {
    id?: number;
    streetNum: string;
    street: string;
    zipCode: string | number;
    city: string;
}

export interface Order {
    id?: number;
    title?: string;
    detail?: string;
    total?: number;
    status?: string;
    lineProducts?: LineProduct[];
}

export interface Category {
    id: number;
    name: string;
    products: Product[];
}

export interface Image {
    id?: number;
    link: string;
}


export interface Comment {
    id?: number;
    rate: number;
    author: string;
    date?: string;
    content: string;
    image?: string;
    linkImgComment?: string;
}

export interface LineProduct {
    id: number;
    title: string;
    quantity: number;
    custom: boolean;
    price: number;
    image: string;
    product: Product;
}

