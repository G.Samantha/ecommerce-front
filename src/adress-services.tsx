import { Adress, User } from "./entities";
import axios from "axios";

export async function fetchAdress(id:number) {
    const response = await axios.get<Adress>('/api/adress/'+id);
    
    return response.data;
}

export async function fetchAllAdresses() {
    const response = await axios.get<Adress[]>('/api/adress/')
    return response.data;
}


export async function updateUser(User:User) {
    const response = await axios.patch<User>('/api/user/'+User.id, User);
    return response.data;
}


export async function updateAdress(Adress:Adress) {
    const response = await axios.patch<Adress>('api/adress/'+Adress.id, Adress);
    return response.data;
}


export async function postUserAdresses(adress:Adress) {
    adress.zipCode = Number(adress.zipCode)
    const response = await axios.post<Adress[]>('/api/account/add', adress);
    return response.data;
}

// export async function deleteAdress(id:any) {
//     const response = await axios.delete<Adress>('api/adress/'+id);
//     return response.data;
// }