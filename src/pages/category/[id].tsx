import { fetchCategoryProducts, fetchOneCategory } from "@/category-services";
import { Category, Product } from "@/entities";
import { GetServerSideProps } from "next";
import Link from "next/link";

interface Props {
  category: Category;
  products: Product[];
}

export default function OneCategory({ category, products }: Props) {
  return (
    <>
      <h1 className="text-center mb-5 mt-5"> {category.name}</h1>

      <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 g-4 justify-content-center">
        {products.map((product) => (
          <div className="card border border-0" style={{ width: 18 + "rem" }}>
            <Link className="my-link" href={"/product/" + product.id}>
              {product.images?.length ? (
                <img
                  className="card-img-top"
                  key={product.id}
                  src={product.images[0].link}
                  alt="img-product"
                />
              ) : (
                <img
                  src="https://kitedanmarkm1.b-cdn.net/media/catalog/product/cache/10/image/500x500/e4d92e6aceaad517e7b5c12e0dc06587/s/k/skateboard_skateboard_nkx_signature_8inch_blue-pink_00.jpg"
                  alt="img-product"
                />
              )}

              <div className="card-body">
                <p className="card-text d-flex justify-content-center">
                  {product.title}
                </p>
                <p className="card-text d-flex justify-content-center">
                  {product.price} €
                </p>
              </div>
            </Link>
          </div>
        ))}
      </div>
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async (
  context
) => {
  const { id } = context.query;

  try {
    return {
      props: {
        category: await fetchOneCategory(Number(id)),
        products: await fetchCategoryProducts(Number(id)),
      },
    };
  } catch {
    return {
      notFound: true,
    };
  }
};
