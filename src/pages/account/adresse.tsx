import Sidebar from "@/components/SideBar";
import { Adress, User } from "@/entities";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { fetchUser, fetchUserAdresses } from "@/auth/auth-service";
import { updateAdress } from "@/adress-services";
import FormAdress from "@/components/FormAdress";

export default function Adresse() {
  const [user, setUser] = useState<User>();
  const [adresses, setAdresses] = useState<Adress[]>();
  const [showEdit, setShowEdit] = useState(false);
  const router = useRouter();

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          router.push("/login");
        }
      });
  }, []);

  useEffect(() => {
    fetchUser()
      .then((data) => setUser(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          router.push("/404");
        }
      });

    fetchUserAdresses()
      .then((data) => setAdresses(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);

  async function updateAdresses() {
    fetchUserAdresses()
      .then((data) => setAdresses(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }

  async function update(adresses: Adress) {
    const updated = await updateAdress(adresses);
    updateAdresses();
  }

  // async function remove(adresses: Adress) {
  //   await deleteAdress(adresses);
  //   router.push("/api/adress/");
  // }

  function toggle() {
    setShowEdit(!showEdit);
  }

  return (
    <>
      <div className="container-fluid">
        <div className="d-flex">
          <Sidebar />

          <div className="d-flex flex-column">
            <div className="py-3">
              <h1>Mes adresses</h1>

              <div className="card m-3" style={{ width: 18 + "rem" }}>
                <div className="card-body bg-light">
                  {adresses?.map((adresse) => (
                    <>
                      <p key={adresse.id}></p>
                      <p className="mb-0">Adresse</p>

                      N° {adresse.streetNum}  {adresse.street}
                      <p className="mb-0">
                      {adresse.zipCode} - {adresse.city}
                      </p>
                    </>
                  ))}
                </div>
              </div>
            </div>

            <div className="container d-flex">
              <div className="m-1">
                <button className="btn greyBackground btn-outline-dark">
                  <a href={"/account/add/"} className="my-link">
                    Ajouter une adresse
                  </a>
                </button>
              </div>

              <div className="m-1">
                <button
                  className="btn greyBackground btn-outline-dark"
                  onClick={toggle}
                >
                  Edit
                </button>
              </div>
            </div>

            <div className="container-fluid">
              <div className="row d-flex justify-content-center">
                {showEdit &&
                  adresses?.map((adresses) => (
                    <FormAdress edited={adresses} onSubmit={update} />
                  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
