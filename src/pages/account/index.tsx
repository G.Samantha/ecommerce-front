import Sidebar from "@/components/SideBar";
import { Adress, User } from "@/entities";
import { useEffect, useState} from "react";
import { useRouter } from "next/router";
import { fetchUser, fetchUserAdresses} from "@/auth/auth-service";


export default function UserPage() {
  const [user, setUser] = useState<User>();
  const [adresses, setAdresses] = useState<Adress[]>();
  const router = useRouter();

  useEffect(() => {
    fetchUser().then(data => {
        setUser(data);
    }).catch(error => {
        if(error.response.status == 401) {
            router.push('/login')
        }
    });
}, [])

  useEffect(() => {
    fetchUser()
      .then((data) => setUser(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          router.push("/404");
        }
      });
      fetchUserAdresses().then((data) => setAdresses(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);

  return (
    <>
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <Sidebar />
          <div className="col py-3">
            <h2>Mon compte</h2>

            <div className="card m-3" style={{ width: 18 + "rem" }}>
              <div className="card-body bg-light">
                <p className="mb-0">
                  {user?.name} {user?.lastname}
                </p>
                <p className="mb-0">E-mail: {user?.mail} </p>
                <p>Téléphone : {user?.phone}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
