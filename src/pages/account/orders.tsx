import Sidebar from "@/components/SideBar";
import { Order, User } from "@/entities";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { fetchUser, fetchUserorder } from "@/auth/auth-service";
import CardOrder from "@/components/CardOrder";

export default function Orders() {
  const [user, setUser] = useState<User>();
  const [orders, setOrders] = useState<Order[]>();
  const [show, setShow] = useState(false);
  const router = useRouter();

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          router.push("/login");
        }
      });
  }, []);

  useEffect(() => {
    fetchUser()
      .then((data) => setUser(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          router.push("/404");
        }
      });

    fetchUserorder()
      .then((data) => setOrders(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);


  return (
    <>
      <div className="container-fluid">
        <div className="d-flex">
          <Sidebar />
          <div className="d-flex flex-column">
            <div className="py-3">
              <h1>Mes commandes</h1>
              {orders?.map((item) => (
                <CardOrder key={item.id} order={item}/>
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
