import { postUserAdresses } from "@/adress-services";
import { fetchUser } from "@/auth/auth-service";
import FormAddAdress from "@/components/FormAddAdress";
import Sidebar from "@/components/SideBar";
import { Adress, User } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function addAdress() {
  const router = useRouter();
  const [errors, setErrors] = useState("");
  const [user, setUser] = useState<User>();

useEffect(() => {
    fetchUser().then(data => {
        setUser(data);
    }).catch(error => {
        if(error.response.status == 401) {
            router.push('/login')
        }
    });
}, [])

  async function handleSubmit(adress: Adress) {
    setErrors('');
    try {
        await postUserAdresses(adress);
    } catch (error: any) {
      console.log(error);

      if (error.response.status == 400) {
        setErrors(error.response.data.detail);
      }
      else{
        console.log(error);
      }
    }
  }


  return (
    <>
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <Sidebar />
          <div className="col py-3">
            <div className="m-3">
              <div className="bgFormImg">
                <h1 className="titreForm">Ajouter une adresse</h1>
                <FormAddAdress onSubmit={handleSubmit} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
