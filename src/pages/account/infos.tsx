import Sidebar from "@/components/SideBar";
import { Adress, User } from "@/entities";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import FormEditUserAccount from "@/components/FormEditUserAccount";
import { fetchUser, fetchUserAdresses } from "@/auth/auth-service";
import { updateUser } from "@/adress-services";

export default function UserInfo() {
  const [user, setUser] = useState<User>();
  const [showEdit, setShowEdit] = useState(false);
  const router = useRouter();

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          router.push("/login");
        }
      });
  }, []);

  useEffect(() => {
    fetchUser()
      .then((data) => setUser(data))
      .catch((error) => {
        console.log(error);
        if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);

  function toggle() {
    setShowEdit(!showEdit);
  }

  async function update(user: User) {
    const updated = await updateUser(user);
    setUser(updated);
  }

  return (
    <>
      <div className="container-fluid">
        <div className="d-flex">
          <Sidebar />

          <div className="d-flex flex-column m-4">
            <div className="py-3">
              <h1>Mes informations</h1>

              <div className="card m-3" style={{ width: 18 + "rem", minWidth:"auto" }}>
                <div className="card-body bg-light">
                  <p className="mb-0">
                    {user?.name}-{user?.lastname}
                  </p>
                  <p className="mb-0">E-mail: {user?.mail} </p>
                  <p>Téléphone : {user?.phone}</p>
                </div>
              </div>
            </div>

            <div className="m-1">
              <button
                className="btn greyBackground btn-outline-dark"
                onClick={toggle}
              >
                Edit
              </button>
            </div>

            <div className="container-fluid">
              <div className="row d-flex justify-content-center">
                {showEdit && (
                  <FormEditUserAccount edited={user} onSubmit={update} />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
