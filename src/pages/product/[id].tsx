
import { AuthContext } from "@/auth/auth-context";
import { addtoCart } from "@/cart-service";
import { postComment } from "@/comment-service";
import Comments from "@/components/Comments";
import FormComment from "@/components/FormComment";
import ItemProduct from "@/components/ItemProduct";
import {  Product, Comment} from "@/entities"
import { fetchOneProduct, fetchProductComments } from "@/product-service";
import { Button } from "@mui/material";
import { GetServerSideProps } from "next"
import { useRouter } from "next/router";
import { useContext, useState } from "react";


interface Props {
    product: Product; 
    comments: Comment[],
}

export default function OneProduct({product, comments:commentsProps }: Props) {
    const [comments,setComments] = useState(commentsProps);
    const {token} = useContext(AuthContext);
    const [showEdit, setShowEdit] = useState(false);
    const router = useRouter();
    
    async function addComment(comment: Comment): Promise<void>  {
        await postComment(comment,product);
        router.push('/');
    }

    
    function toggle() {
        setShowEdit(!showEdit);
    }

    return (
        <>
        <h1 className="text-center mb-5 mt-5"> {product.title}</h1>
        <div className="row">
        <ItemProduct product={product} onAction={()=> {addtoCart(product); router.push('/cart');}} /> 
        </div>
        <Comments comments={comments} />
        <div className="row justify-content-center">
        <div  className="col-10">
            {token && 
            <Button color="info" variant="contained" type="button" className="btn btn-primary mt-4 mb-4"  onClick={toggle}>Ajouter un commentaire</Button>
            }
            
            </div>
            </div>
            {showEdit &&
            <FormComment onSubmit={addComment}/>
        }
        </>
        
        )
    }
    
    export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
        const { id } = context.query;
        
        try {
            
            return {
                props: {
                    product: await fetchOneProduct(Number(id)),
                    comments: await fetchProductComments(Number(id)),
                }
            }
            
        } catch {
            return {
                notFound: true
            }
        }
        
    }

