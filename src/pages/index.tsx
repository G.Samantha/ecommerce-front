
import React from 'react'
import ArticlesPopAccueil from "@/components/ArticlesPopAccueil";
import { Category, Product } from "../entities";
import { fetchAllProduct } from "@/product-service";
import { GetServerSideProps } from "next";
import { fetchAllCategory } from "@/category-services";
import CarrousselAccueil from "@/components/CarrousselAccueil";
import CategorysAccueil from "@/components/CategorysAccueil";

interface Props {
  products: Product[];
  categorys: Category[];
}

export default function Index({ products, categorys }: Props) {
  const firstFourElements = products.slice(0, 5);
  return (
    <>
      <div className="row">
        <div className="col">
          <CarrousselAccueil />
        </div>
      </div>

      <h2 className="d-flex justify-content-center mt-5 mb-5">
        Articles les plus populaires
      </h2>
      <div className="articlespop row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 g-4 justify-content-center">
        {firstFourElements.map((item) => (
          <ArticlesPopAccueil key={item.id} products={item} />
        ))}
      </div>

      <div className="row sectionarticle">
        <h3 className="text-center">Nos produits</h3>
      </div>

      <CategorysAccueil categorys={categorys} />
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  return {
    props: {
      products: await fetchAllProduct(),
      categorys: await fetchAllCategory(),
    },
  };
};
